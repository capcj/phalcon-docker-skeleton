<?php declare(strict_types = 1);

/**
 * Environment Config
 **/

return [
    'database' => [
        'host'     => 'localhost',
        'username' => 'scott',
        'password' => 'cheetah',
        'dbname'   => 'test_db'
    ],
     'app' => [
        'namespace' => 'Api',
        'controllersDir' => 'app/controllers/',
        'entitiesDir' => 'lib/api/entities/',
        'repositoriesDir' => 'lib/api/repositories/'
    ],
];
