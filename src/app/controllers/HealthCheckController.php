<?php declare(strict_types = 1);

namespace Api\Controllers;

use Api\Controllers\Core\Controller;

class HealthCheckController extends Controller
{
    public function indexAction()
    {
        echo $this->toJson(1);
    }
}
