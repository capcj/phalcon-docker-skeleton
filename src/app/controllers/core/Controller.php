<?php declare(strict_types = 1);

namespace Api\Controllers\Core;

use Phalcon\Http\Response;
use Phalcon\Mvc\Controller as BaseController;

class Controller extends BaseController
{
    public function toJson($result, int $code = 200): void
    {
        (new Response())->setStatusCode($code)->setJsonContent($result)->send();
    }
}
