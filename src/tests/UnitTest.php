<?php

namespace Tests;

/**
 * Class UnitTest
 */
class UnitTest extends UnitTestCase
{
    public function testTestCase(): void
    {
        $this->assertSame(
            "works",
            "works",
            "This is OK"
        );

        $this->assertNotSame(
            "works",
            "works1"
        );
    }
}
