<?php declare(strict_types = 1);

use Phalcon\Config;
use Phalcon\Di\FactoryDefault;
use Phalcon\Loader;
use Phalcon\Mvc\Application;
use Phalcon\Mvc\Dispatcher;
use Phalcon\Mvc\Url;
use Phalcon\Mvc\View;

if (isset($_GET['debug']) && $_GET['debug'] === '1') {
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
}

/**
 * DEPENDENCY INJECTION
 **/

$di = new FactoryDefault();


$di->set(
    'config',
    function () {
        $configData = require 'app/config/config.php';

        return new Config($configData);
    }
);

// Register the default dispatcher's namespace for controllers
$di->set(
    'dispatcher',
    function () use ($di) {
        $dispatcher = new Dispatcher();

        $dispatcher->setDefaultNamespace(
            $di->get('config')->app->namespace . '\Controllers'
        );

        return $dispatcher;
    }
);

// Setup a base URI
$di->set(
    'url',
    function () {
        $url = new Url();
        $url->setBaseUri('/');
        return $url;
    }
);

// Registering the view component
$di->set(
    'view',
    function () {
        $view = new View();
        $view->setRenderLevel(View::LEVEL_NO_RENDER);
        return $view;
    }
);

/**
 * LOADER
 **/

$loader = new Loader();

// Use autoloading with namespaces prefixes
$loader->registerNamespaces(
    [
        $di->get('config')->app->namespace . '\Controllers' => $di->get('config')->app->controllersDir,
        $di->get('config')->app->namespace . '\Controllers\Core' => $di->get('config')->app->controllersDir . '/core/',
        'Lib\Api\Entities' => $di->get('config')->app->entitiesDir,
        'Lib\Api\Repositories' => $di->get('config')->app->repositoriesDir,
    ]
);

/**
 * Register Files, composer autoloader
 */
$loader->registerFiles(
    [
        'vendor/autoload.php'
    ]
);

$loader->register();

/**
 * APPLICATION
 **/

$application = new Application($di);

try {
    $response = $application->handle();

    $response->send();
} catch (\Exception $e) {
    echo json_encode(['error' => $e->getMessage()]);
}
