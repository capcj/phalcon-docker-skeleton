FROM alpine:latest
MAINTAINER CA <capcj@protonmail.com>

ENV PHALCON_DEV_TOOLS_VERSION=3.4.3

# Compile Phalcon
RUN set -xe \
        && apk --update --no-cache add autoconf openrc g++ make curl \
           php7 php7-fpm php7-common php7-gd php7-pdo php7-mysqli php7-zlib php7-curl php7-phalcon \
        # Install Phalcon Devtools, see https://github.com/phalcon/phalcon-devtools/
        && curl -LO https://github.com/phalcon/phalcon-devtools/archive/v${PHALCON_DEV_TOOLS_VERSION}.tar.gz \
        && tar xzf v${PHALCON_DEV_TOOLS_VERSION}.tar.gz \
        && mv phalcon-devtools-${PHALCON_DEV_TOOLS_VERSION} /usr/local/phalcon-devtools \
        && ln -s /usr/local/phalcon-devtools/phalcon.php /usr/local/bin/phalcon \
        && sed -i 's/127.0.0.1/0.0.0.0/g' /etc/php7/php-fpm.d/www.conf \
        && rm -rf /var/cache/apk/*

EXPOSE 9000

WORKDIR /src

#ENTRYPOINT ["top"]
ENTRYPOINT ["php-fpm7", "-F" , "-R"]
